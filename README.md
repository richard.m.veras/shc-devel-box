Vagrant is a tool that address the issue of "well my code worked on my machine" by describing the provisioning of development environments in a reproducible manner. This Vagrantfile provides the entire development environment for SHC CS. Once it is installed you should be able to access a Linux environment, via command prompt or GUI, with all the development tools installed and configured.

# Installation
There are two routes to install this environment.
- (Easy) Install VirtualBox and Vagrant then run `vagrant up`
- (Customizable) Install Linux on a machine, virtual or physical, then run `./provision.sh`

## Installation on a Mac
These instructions are adapted from [here](https://sourabhbajaj.com/mac-setup/Vagrant/README.html)

First install `homebrew` following the instructions [here](https://brew.sh/)

1. open a mac terminal and type the following to install `homebrew`
`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
2. install VirtualBox via `homebrew`
`brew cask install virtualbox`
3. install Vagrant and Vagrant Manager via `homebrew`
`brew cask install vagrant`
`brew cask install vagrant-manager`

## Installation on Windows


## Installation on Linux


# Running the Vagrantfile

1. clone or copy this repository into a folder
2. from the terminal navigate to that folder
3. create the Vagrant box by running the following command in that folder
`vagrant up`

4. You can then access the machine via a Graphical User Interface (GUI) by running the virtualbox program and selecting the machine labelled `shc_devel_box`

Alternatively, you can access the machine via the terminal by typing:
`vagrant ssh`


