#!/bin/bash

apt-get update
apt-get upgrade -y

# TODO: I should be specific about which version of java to use.
apt-get install -y default-jre  default-jdk

snap install --classic code

java --version
javac --version

# https://stackoverflow.com/questions/34286515/how-to-install-visual-studio-code-extensions-from-command-line
code --install-extension vscjava.vscode-java-pack
code --list-extensions


